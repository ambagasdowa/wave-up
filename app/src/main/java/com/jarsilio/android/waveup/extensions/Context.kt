package com.jarsilio.android.waveup.extensions

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.os.Build
import com.jarsilio.android.waveup.prefs.Settings
import com.jarsilio.android.waveup.receivers.LockScreenAdminReceiver
import com.jarsilio.android.waveup.service.WaveUpWorldState
import timber.log.Timber

val Context.settings: Settings
    get() = Settings.getInstance(this)

val Context.state: WaveUpWorldState
    get() = WaveUpWorldState.getInstance(this)

val Context.isOreoOrNewer: Boolean
    get() = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

val Context.isPieOrNewer: Boolean
    get() = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P

fun Context.removeDeviceAdminPermission() {
    Timber.i("Removing lock screen admin rights forcing 'lockScreen' to false.")
    val devAdminReceiver = ComponentName(this, LockScreenAdminReceiver::class.java)
    val dpm = this.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
    dpm.removeActiveAdmin(devAdminReceiver)

    // If the user cancels the uninstall he/she will have to switch it back on (to request the admin rights again)
    this.settings.isLockScreen = false
}