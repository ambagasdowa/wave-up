package com.jarsilio.android.waveup

import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.text.Html
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.TextView

class ImpressumActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_html_abstract)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        // Add back button
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }

        val textView = TextView(this)
        textView.text = fromHtml(getString(R.string.impressum_activity_text))
        textView.movementMethod = LinkMovementMethod.getInstance()

        val linearLayout = findViewById<LinearLayout>(R.id.simple_html_abstract_linear_layout)
        linearLayout.addView(textView)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            super.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private fun fromHtml(source: String): Spanned {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {

                Html.fromHtml(source)
            }
        }
    }
}